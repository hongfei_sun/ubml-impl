/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.variable;

import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.util.DataValidator;

/**
 * The Collectioin Of Common Variable
 *
 * @ClassName: CommonVariableCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableCollection extends GspFieldCollection {
    private static final long serialVersionUID = 1L;
    private CommonVariableEntity parentObject;

    /**
     * 创建字段集合
     *
     * @param parentObj
     */
    public CommonVariableCollection(CommonVariableEntity parentObj) {
        super(parentObj);
        parentObject = parentObj;
    }

    /**
     * 克隆
     *
     * @param absObject
     * @param parentAssociation
     * @return
     */
    public CommonVariableCollection clone(CommonVariableEntity absObject, GspAssociation parentAssociation) {
        CommonVariableCollection newCollection = new CommonVariableCollection(absObject);
        for (IGspCommonField item : this) {
            newCollection.add(((CommonVariable)item).clone(absObject, parentAssociation));
        }
        return newCollection;
    }

    /**
     * Index[string]
     */
    public final CommonVariable getItem(String id) {
        return (CommonVariable)super.getByID(id);
    }

    @Override
    public final CommonVariable getItem(int index) {
        return (CommonVariable)super.get(index);
    }

    /**
     * 所属结点
     */
    @Override
    public CommonVariableEntity getParentObject() {
        return parentObject;
    }

    public final void setParentObject(CommonVariableEntity value) {
        super.setParentObject(value);
    }

    /**
     * 添加一个新字段
     *
     * @param element
     */
    public final void addField(CommonVariable element) {
        DataValidator.checkForNullReference(element, "element");

        super.add(element);
        if (getParentObject() != null) {
            element.setBelongObject(getParentObject());
        }
    }

    /**
     * 移动字段
     *
     * @param toIndex 目的索引为止
     * @param element 需要移动的字段
     */
    public final void move(int toIndex, CommonVariable element) {
        if (contains(element)) {
            super.move(toIndex, element);
        }
    }
}