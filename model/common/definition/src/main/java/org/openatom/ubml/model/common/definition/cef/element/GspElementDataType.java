/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.element;

/**
 * The Definition Of ElementDataType
 *
 * @ClassName: ElementDataType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspElementDataType {
    /**
     * 字符串
     */
    String(0),
    /**
     * 长文本
     */
    Text(1),
    /**
     * 整形
     */
    Integer(2),
    /**
     * 十进制
     */
    Decimal(3),
    /**
     * 布尔型
     */
    Boolean(4),
    /**
     * 日期型
     */
    Date(5),
    /**
     * 日期时间型
     */
    DateTime(6),
    /**
     * 二进制
     */
    Binary(7);

    private static java.util.HashMap<Integer, GspElementDataType> mappings;
    private int intValue;

    private GspElementDataType(int value) {
        intValue = value;
        GspElementDataType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, GspElementDataType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspElementDataType>();
        }
        return mappings;
    }

    public static GspElementDataType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}