/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import org.openatom.ubml.model.common.definition.cef.element.GspAssociationKey;

/**
 * The Collection Of AssociationKey
 *
 * @ClassName: GspAssociationKeyCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationKeyCollection extends BaseList<GspAssociationKey> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * 创建关联关系外键集合
     */
    public GspAssociationKeyCollection() {
    }

    /**
     * add AssociationKey
     *
     * @param associationKey
     */
    public final void addAssociation(GspAssociationKey associationKey) {
        super.add(associationKey);
    }

    /**
     * remove AssociationKey
     *
     * @param associationKey
     */
    public final void remove(GspAssociationKey associationKey) {
        if (contains(associationKey)) {
            super.remove(associationKey);
        }
    }

    /**
     * removeAt
     *
     * @param index
     */
    @Override
    public void removeAt(int index) {
        remove(index);
    }

    /**
     * Insert
     *
     * @param index
     * @param associationKey
     */
    @Override
    public final void insert(int index, GspAssociationKey associationKey) {
        super.insert(index, associationKey);
    }

}