/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.entity;

import java.util.ArrayList;
import java.util.List;
import org.openatom.ubml.model.common.definition.cef.IGspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.collection.CommonDtmCollection;
import org.openatom.ubml.model.common.definition.cef.collection.CommonValCollection;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.util.Guid;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonDataType implements IGspCommonDataType {
    // 私有属性
    private String id = Guid.newGuid().toString();
    private String code = "";
    private String name = "";
    private CommonDtmCollection dtmBeforeSave = new CommonDtmCollection();
    private CommonDtmCollection dtmAfterCreate = new CommonDtmCollection();
    private CommonDtmCollection dtmAfterModify = new CommonDtmCollection();
    private CommonValCollection valBeforeSave = new CommonValCollection();
    private CommonValCollection valAfterModify = new CommonValCollection();
    private GspFieldCollection containElements = new GspFieldCollection();
    private CustomizationInfo customizationInfo = new CustomizationInfo();
    private List<String> beLabel;
    private List<String> bizTagIds;
    private Boolean bigNumber;


    /**
     * 是否引用对象
     */
    private boolean privateIsRef;


    // 公共属性
    /**
     * 国际化项前缀
     */
    private String privateI18nResourceInfoPrefix;

    // 构造函数
    protected GspCommonDataType() {
        setContainElements(new GspFieldCollection());
        // DynamicPropSerializerComps = new List<MdRefInfo>();

        setIsRef(false);
    }

    /**
     * 唯一标识
     */
    public String getID() {
        return id;
    }

    public void setID(String value) {
        id = value;
    }

    /**
     * 编码
     */
    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        code = value;
    }

    /**
     * 名称
     */
    public String getName() {
        return name;
    }

    public void setName(String value) {
        name = value;
    }

    ;

    /**
     * 标签
     *
     * @return
     */
    public List<String> getBeLabel() {
        if (beLabel == null) {
            beLabel = new ArrayList<String>();
            return beLabel;
        }
        return beLabel;
    }

    public void setBeLabel(List<String> value) {
        beLabel = value;
    }

    ;

    public List<String> getBizTagIds() {
        if (bizTagIds == null) {
            bizTagIds = new ArrayList<String>();
            return bizTagIds;
        }
        return bizTagIds;
    }

    public void setBizTagIds(List<String> value) {
        bizTagIds = value;
    }

    public final boolean getIsRef() {
        return privateIsRef;
    }

    public final void setIsRef(boolean value) {
        privateIsRef = value;
    }

    /**
     * bigNumber
     */
    public boolean isBigNumber() {
        return this.bigNumber;
    }

    /**
     * 当前节点字段集合
     *
     */
    // private IFieldCollection IGspCommonDataType.ContainElements=>

    // getContainElements();

    public void setIsBigNumber(boolean value) {
        this.bigNumber = value;
    }

    /**
     * 当前节点字段集合
     */
    public GspFieldCollection getContainElements() {
        return containElements;
    }

    public void setContainElements(GspFieldCollection value) {
        this.containElements = value;
    }

    /**
     * 保存前联动计算
     */
    public CommonDtmCollection getDtmBeforeSave() {
        return dtmBeforeSave;
    }

    public void setDtmBeforeSave(CommonDtmCollection value) {
        dtmBeforeSave = value;
    }

    /**
     * 更新后联动计算
     */
    public CommonDtmCollection getDtmAfterModify() {
        return dtmAfterModify;
    }

    public void setDtmAfterModify(CommonDtmCollection value) {
        dtmAfterModify = value;
    }

    /**
     * 新建后联动计算
     */
    public CommonDtmCollection getDtmAfterCreate() {
        return dtmAfterCreate;
    }

    public void setDtmAfterCreate(CommonDtmCollection value) {
        dtmAfterCreate = value;
    }

    /**
     * 保存前校验规则
     */
    public CommonValCollection getValBeforeSave() {
        return valBeforeSave;
    }

    public void setValBeforeSave(CommonValCollection value) {
        valBeforeSave = value;
    }

    /**
     * 更新后校验规则
     */
    public CommonValCollection getValAfterModify() {
        return valAfterModify;
    }

    public void setValAfterModify(CommonValCollection value) {
        valAfterModify = value;
    }

    public final String getI18nResourceInfoPrefix() {
        return privateI18nResourceInfoPrefix;
    }

    public final void setI18nResourceInfoPrefix(String value) {
        privateI18nResourceInfoPrefix = value;
    }

    /**
     * 运行时定制信息
     */
    public CustomizationInfo getCustomizationInfo() {
        return customizationInfo;
    }

    public void setCustomizationInfo(CustomizationInfo customizationInfo) {
        this.customizationInfo = customizationInfo;
    }


    // 方法
    public abstract IGspCommonField findElement(String id);

    public abstract ClassInfo getGeneratedEntityClassInfo();



    /**
     * 根据标签获取字段列表
     *
     * @return
     */
    public List<IGspCommonField> getFieldsByBeLabel() {
        List<IGspCommonField> list = new ArrayList<>();
        for (IGspCommonField item : getContainElements()) {
            if (item.getBeLabel().size() > 0) {

                list.add((GspCommonField)item);

            }
        }
        return list;
    }
}
