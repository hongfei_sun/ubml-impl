/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.merger;

import org.openatom.ubml.model.common.definition.cef.i18n.context.CefResourcePrefixInfo;
import org.openatom.ubml.model.common.definition.cef.i18n.context.ICefResourceMergeContext;

public abstract class AbstractResourceMerger {
    protected AbstractResourceMerger(
        ICefResourceMergeContext context, CefResourcePrefixInfo parentResourceInfo) {
        this.context = context;
        this.parentResourcePrefixInfo = parentResourceInfo;
    }

    protected AbstractResourceMerger(ICefResourceMergeContext context) {
        this.context = context;
    }

    private ICefResourceMergeContext context;

    protected final ICefResourceMergeContext getContext() {
        return context;
    }

    private CefResourcePrefixInfo parentResourcePrefixInfo;

    protected final CefResourcePrefixInfo getParentResourcePrefixInfo() {
        return parentResourcePrefixInfo;
    }

    public void merge() {
        mergeItems();
    }

    protected abstract void mergeItems();

}
