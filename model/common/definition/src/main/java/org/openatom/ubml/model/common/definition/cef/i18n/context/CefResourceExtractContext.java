/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.context;

import org.openatom.ubml.model.common.definition.cef.util.DataValidator;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

/**
 * 多语资源项抽取上下文
 */
public class CefResourceExtractContext implements ICefResourceExtractContext {
    private String metaPrefix;
    private I18nResourceItemCollection resourceItems;

    public CefResourceExtractContext(String metaPrefix, I18nResourceItemCollection items) {
        DataValidator.checkForEmptyString(metaPrefix, "元数据前缀");
        DataValidator.checkForNullReference(items, "国际化资源项");
        this.metaPrefix = metaPrefix;
        this.resourceItems = items;
    }

    /**
     * 获取前缀
     *
     * @return
     */
    public final String getKeyPrefix() {
        return metaPrefix;
    }

    /**
     * 新增/修改
     *
     * @param info
     */
    public final void setResourceItem(CefResourceInfo info) {
        String key = info.getResourceKey();
        String value = info.getResourceValue();
        String description = info.getDescription();

        I18nResourceItem resource = GetNewResource(key, value, description);

        resourceItems.add(resource);
    }

    private I18nResourceItem GetNewResource(String key, String value, String description) {
        I18nResourceItem resourceItem = new I18nResourceItem();
        resourceItem.setKey(key);
        resourceItem.setValue(value);
        resourceItem.setComment(description);
        return resourceItem;
    }
}