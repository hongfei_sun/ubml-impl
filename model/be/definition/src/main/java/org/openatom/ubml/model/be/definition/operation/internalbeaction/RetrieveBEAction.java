/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.internalbeaction;

import org.openatom.ubml.model.be.definition.operation.BizAction;

/**
 * The Internal Retrieve Action  Definition
 *
 * @ClassName: RetrieveBEAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class RetrieveBEAction extends BizAction implements IInternalBEAction {
    
    public static final String ID = "cJwzDlFfa0uz4rChlTEd4g";
    
    public static final String CODE = "Retrieve";
    
    public static final String NAME = "内置检索操作";

    public RetrieveBEAction() {
        setID(ID);
        setCode(CODE);
        setName(NAME);
    }
}