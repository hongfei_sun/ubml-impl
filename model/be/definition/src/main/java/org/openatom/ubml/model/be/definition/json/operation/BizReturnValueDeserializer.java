/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizParameter;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizReturnValue;
import org.openatom.ubml.model.be.definition.operation.componentbase.BizVoidReturnType;

/**
 * The  Josn Seserializer Of Return Value Of Actions
 *
 * @ClassName: BizReturnValueDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizReturnValueDeserializer extends BizParaDeserializer {
    @Override
    public final BizReturnValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        BizReturnValue rez = (BizReturnValue)super.deserialize(jsonParser, null);
        if (BizVoidReturnType.ASSEMBLY.equals(rez.getAssembly())
                && BizVoidReturnType.CLASS_NAME.equals(rez.getClassName())) {
            BizReturnValue newRez = new BizVoidReturnType();
            newRez.setID(rez.getID());
            newRez.setParamCode(rez.getParamCode());
            newRez.setParamDescription(rez.getParamDescription());
            newRez.setParameterType(rez.getParameterType());
            return newRez;
        }
        return rez;
    }

    @Override
    protected BizParameter createBizPara() {
        return new BizReturnValue();
    }
}
