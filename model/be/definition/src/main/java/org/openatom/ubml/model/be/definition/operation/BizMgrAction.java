/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.openatom.ubml.model.be.definition.beenum.BEOperationType;
import org.openatom.ubml.model.be.definition.json.operation.BizMgrActionDeserializer;
import org.openatom.ubml.model.be.definition.json.operation.BizMgrActionSerializer;
import org.openatom.ubml.model.be.definition.operation.bemgrcomponent.BizMgrActionParamCollection;

/**
 * The Definition Of Manager Action
 *
 * @ClassName: BizMgrAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = BizMgrActionSerializer.class)
@JsonDeserialize(using = BizMgrActionDeserializer.class)
public class BizMgrAction extends BizActionBase implements Cloneable {
    /**
     * 业务操作ID
     */
    private String privateFuncOperationID;
    /**
     * 业务操作名称, 显示用
     */
    private String privateFuncOperationName;
    private BizMgrActionParamCollection beMgrParams;

    public BizMgrAction() {
        super();
        beMgrParams = new BizMgrActionParamCollection();
    }

    public final String getFuncOperationID() {
        return privateFuncOperationID;
    }

    public final void setFuncOperationID(String value) {
        privateFuncOperationID = value;
    }

    public final String getFuncOperationName() {
        return privateFuncOperationName;
    }

    public final void setFuncOperationName(String value) {
        privateFuncOperationName = value;
    }

    /**
     * 操作类型
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.BizMgrAction;
    }

    @Override
    protected BizMgrActionParamCollection getActionParameters() {
        return beMgrParams;
    }
}