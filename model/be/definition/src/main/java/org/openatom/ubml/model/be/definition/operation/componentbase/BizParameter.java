/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation.componentbase;

import org.openatom.ubml.model.be.definition.operation.componentenum.BizCollectionParameterType;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterMode;
import org.openatom.ubml.model.be.definition.operation.componentenum.BizParameterType;
import org.openatom.ubml.model.be.definition.operation.componentinterface.IBizParameter;

/**
 * The Definition Of Biz Parameter
 *
 * @ClassName: BizParameter
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class BizParameter implements IBizParameter {
    private String privateID;
    /**
     * 参数名
     */
    private String privateParamCode;
    /**
     * 参数名
     */
    private String privateParamName;
    /**
     * 参数类型，与Assembly和ClassName关联
     */
    private BizParameterType privateParameterType = BizParameterType.forValue(0);
    /**
     * 参数类对应程序集类
     */
    private String privateAssembly;
    /**
     * 参数类名
     */
    private String privateClassName;
    private String netClassName;
    /**
     * 参数模式
     */
    private BizParameterMode privateMode = BizParameterMode.forValue(0);
    /**
     * 描述
     */
    private String privateParamDescription;
    private BizCollectionParameterType privateCollectionParameterType = BizCollectionParameterType.forValue(0);

    public String getID() {
        return privateID;
    }

    public void setID(String value) {
        privateID = value;
    }

    public String getParamCode() {
        return privateParamCode;
    }

    public void setParamCode(String value) {
        privateParamCode = value;
    }

    public String getParamName() {
        return privateParamName;
    }

    public void setParamName(String value) {
        privateParamName = value;
    }

    public BizParameterType getParameterType() {
        return privateParameterType;
    }

    public void setParameterType(BizParameterType value) {
        privateParameterType = value;
    }

    public String getAssembly() {
        return privateAssembly;
    }

    public void setAssembly(String value) {
        privateAssembly = value;
    }

    public String getClassName() {
        return privateClassName;
    }

    public void setClassName(String value) {
        privateClassName = value;
    }

    public String getNetClassName() {
        return netClassName;
    }

    public void setNetClassName(String value) {
        netClassName = value;
    }

    public BizParameterMode getMode() {
        return privateMode;
    }

    public void setMode(BizParameterMode value) {
        privateMode = value;
    }

    public String getParamDescription() {
        return privateParamDescription;
    }

    public void setParamDescription(String value) {
        privateParamDescription = value;
    }

    public final BizCollectionParameterType getCollectionParameterType() {
        return privateCollectionParameterType;
    }

    public final void setCollectionParameterType(BizCollectionParameterType value) {
        privateCollectionParameterType = value;
    }
}