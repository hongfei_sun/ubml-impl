/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.refi18n;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.core.MetadataCoreManager;
import com.inspur.edp.lcm.metadata.core.MetadataProjectCoreService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SelfProjectFilter implements IRefI18nFilter {
    @Override
    public List<GspMetadata> getRefI18nMetadata(String resouceMdID, String resourceFileName, String path,
        String mavenPath, String packagePath) {
        List<GspMetadata> languageMetadataList = new ArrayList<>();

        MetadataProjectCoreService metadataProjectCoreService = new MetadataProjectCoreService();
        MetadataProject metadataProjInfo = metadataProjectCoreService.getMetadataProjInfo(path);
        MetadataCoreManager metadataCoreManager = new MetadataCoreManager();
        GspMetadata resourceMetadata = metadataCoreManager.loadMetadataByMetadataId(resouceMdID, metadataProjInfo.getProjectPath());

        //查找对应的语言包元数据，语言包元数据需要文件操作，获取所有的语言包元数据文件
        //资源元数据以及资源元数据对应的语言包元数据，是平铺到一起的
        //资源元数据文件名称.languagecode.lres(SalesOrder0426.be.en.lres)
        //先过滤包含资源元数据文件名称的文件，然后过滤后缀为lres的文件
        String absolutePath = ManagerUtils.getAbsolutePath(resourceMetadata.getRelativePath());
        FileService fileService = new FileServiceImp();
        String searchFileName = resourceFileName == null || resourceFileName.isEmpty() ? fileService.getFileNameWithoutExtension(resourceMetadata.getHeader().getFileName()) : fileService.getFileNameWithoutExtension(resourceFileName);
        File[] files = new File(absolutePath).listFiles((dir, name) -> name.startsWith(searchFileName) && name.endsWith(Utils.getLanguageMetadataFileExtention()));

        for (File file : files) {
            GspMetadata metadata = metadataCoreManager.loadMetadata(file.getName(), file.getParent());
            languageMetadataList.add(metadata);
        }
        return languageMetadataList;
    }
}
