/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.ref;

import com.inspur.edp.lcm.metadata.api.service.MavenUtilsService;
import com.inspur.edp.lcm.metadata.core.MavenUtilsCore;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;

/**
 * Classname MavenUtils Description mvn操作工具类 Date 2019/12/9 9:49
 *
 * @author zhongchq
 * @version 1.0
 */
public class MavenUtilsImp implements MavenUtilsService {

    @Override
    public boolean exeMavenCommand(String javaCodePath, String goal) {

        String mavenPath = ManagerUtils.getMavenStoragePath();
        MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();
        return mavenUtilsCore.exeMavenCommand(javaCodePath, mavenPath, goal);
    }

    @Override
    public String getLocalRepository() {
        MavenUtilsCore mavenUtilsCore = new MavenUtilsCore();
        return mavenUtilsCore.getLocalRepository();
    }
}
