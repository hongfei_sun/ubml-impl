/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.lcm.metadata.api.entity.MetadataCustomizationFilter;
import com.inspur.edp.lcm.metadata.api.entity.MetadataRTFilter;
import com.inspur.edp.lcm.metadata.api.entity.ServiceUnitInfo;
import java.util.List;

/**
 * 元数据运行时服务接口
 *
 * @author zhaoleitr
 */
public interface MetadataRTService {
    /**
     * 运行时获取元数据（不带国际化信息）
     *
     * @param id 元数据id
     * @return 元数据内容实体
     */
    GspMetadata getMetadataRTByID(String id);

    /**
     * 运行时获取元数据（带国际化信息）
     *
     * @param id 元数据id
     * @return 元数据内容实体
     */
    GspMetadata getMetadata(String id);

    /**
     * 根据过滤条件获取元数据
     *
     * @param metadataCustomizationFilter 元数据过滤器
     * @return 元数据实体
     */
    GspMetadata getMetadata(MetadataCustomizationFilter metadataCustomizationFilter);

    /**
     * 加载服务端所有的元数据
     */
    void loadAllMetadata();

    /**
     * 根据元数据类型获取元数据信息列表
     *
     * @param metadataTypes 元数据类型列表，通过","分隔
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataRefList(String metadataTypes);

    /**
     * 获取所有的元数据列表
     *
     * @return 元数据信息列表
     */
    List<Metadata4Ref> getMetadataRefList();

    /**
     * @param filter 过滤条件
     * @return 元数据信息列表
     * @author zhongchq
     */
    List<Metadata4Ref> getMetadataListByFilter(MetadataRTFilter filter);

    /**
     * 根据业务主键获取元数据
     *
     * @param metadataNamespace 元数据命名空间
     * @param metadataCode      元数据编号
     * @param metadataTypeCode  元数据类型编号
     * @return 元数据内容实体
     */
    GspMetadata getMetadataBySemanticID(String metadataNamespace, String metadataCode, String metadataTypeCode);

    /**
     * 根据元数据ID获取SU信息
     *
     * @param metadataId 元数据id
     * @return 服务单元信息
     */
    ServiceUnitInfo getServiceUnitInfo(String metadataId);
}
