/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.entity.MetadataType;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * @Classname MetadataTypeConfigLoader
 * @Description 元数据类型
 * @Date 2019/7/23 15:03
 * @Created by zhongchq
 * @Version 1.0
 */
public class MetadataTypeConfigLoader implements Serializable {

    private String fileName = "config/platform/common/lcm_metadataextend.json";
    private String sectionName = "MetadataType";

    private List<MetadataType> metadataTypeList;

    public MetadataTypeConfigLoader() {
        loadMetadataConfigurations();
    }

    /**
     * @return java.util.List<com.inspur.gsp.lcm.metadata.entity.MetadataType>
     * @author zhongchq 类初始化时反序列化Json到实体
     **/
    public List<MetadataType> loadMetadataConfigurations() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            if (metadataTypeList != null && metadataTypeList.size() > 0) {
                return metadataTypeList;
            }
            FileService fileService = new FileServiceImp();
            fileName = fileService.getCombinePath(EnvironmentUtil.getServerRTPath(), fileName);
            metadataTypeList = metadataServiceHelper.getMetadataTypeList(fileName, sectionName);
            return metadataTypeList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
