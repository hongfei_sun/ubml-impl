/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.nio.file.Paths;

public class RefStrategyConfigLoader {

    private String refStrategy;
    private String fileName = "config/platform/dev/main/ref_strategy.json";
    private String sectionName = "strategy";
    ObjectMapper objectMapper = new ObjectMapper();

    public RefStrategyConfigLoader() {
    }

    protected String loadRefStrategy() {
        fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();
        try {
            if (refStrategy != null) {
                return refStrategy;
            }
            FileServiceImp fileService = new FileServiceImp();
            String fileContents = fileService.fileRead(fileName);
            JsonNode jsonNode = objectMapper.readTree(fileContents);
            refStrategy = jsonNode.findValue(sectionName).toString();
            return refStrategy;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
