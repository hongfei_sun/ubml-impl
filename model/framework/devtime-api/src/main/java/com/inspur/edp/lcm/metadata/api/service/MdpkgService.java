/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenComponents;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenPackageRefs;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataRepo;
import java.io.IOException;
import java.util.List;

/**
 * 元数据包服务
 *
 * @author zhaoleitr
 */
public interface MdpkgService {

    /**
     * 安装元数据接口。
     *
     * @param groupId         groupId
     * @param artifactId      artifactId
     * @param version         版本
     * @param path            工程路径
     * @param forceUpdateFlag 是否强制更新
     * @throws IOException IO异常
     */
    void installMdpkg(String groupId, String artifactId, String version, String path,
        Boolean forceUpdateFlag) throws IOException;

    /**
     * 卸载元数据接口。
     *
     * @param projPath 工程路径
     * @param packName maven包名
     */
    void uninstallMdpkg(String projPath, String packName);

    /**
     * 部署jar到nexus私服中。 step1:将元数据包打包中jar中。 step2:推送
     *
     * @param proPath           元数据工程路径
     * @param repoId            仓库标识
     * @param dependencyVersion 依赖版本
     */
    void deployMdpkg(String proPath, String repoId, String dependencyVersion);

    /**
     * 获取当前环境中配置的maven配置，从settings.xml配置仓库，settings.xml还得配置用户名与密码
     *
     * @return MetadataRepo 返回列表
     */
    MetadataRepo getRepoList();

    /**
     * 添加工程依赖
     *
     * @param pompath    pom文件路径
     * @param groupId    groupId
     * @param artifactId artifactId
     * @param version    版本
     * @throws IOException IO异常
     */
    void addProjectDependency(String pompath, String groupId, String artifactId, String version) throws IOException;

    /**
     * 编译
     *
     * @param path    工程路径
     * @param modules 编译模块列表
     */
    void compile(String path, String modules);

    /**
     * 编译工程下所有模块
     *
     * @param path 工程路径
     */
    void compileAll(String path);

    /**
     * 重新安装依赖工程
     *
     * @param path  工程路径
     * @param force 是否强制更新 工程路径
     */
    void restore(String path, Boolean force);

    /**
     * 给工程添加依赖并拉取
     *
     * @param path
     * @param metadataPackages
     */
    void addDepedencyAndRestore(String path, List<String> metadataPackages);

    /**
     * 发布正式版本包
     *
     * @param path                工程路径
     * @param version             工程版本号
     * @param repoId              仓库id
     * @param revert              版本撤回
     * @param dependenciesVersion 依赖的统一版本号
     * @throws IOException IO异常
     */
    void deployReleaseMdVersion(String path, String version, String repoId, boolean revert,
        String dependenciesVersion) throws IOException;

    /**
     * 发布临时版本包
     *
     * @param path    工程路径
     * @param version 工程版本号
     * @param repoId  仓库id
     * @param revert  版本撤回
     * @throws IOException IO异常
     **/
    void deploySnapshotMdVersion(String path, String repoId, String version, boolean revert) throws IOException;

    /**
     * 获取maven包最新版本
     *
     * @param groupId    groupId
     * @param artifactId artifactId
     * @return maven包最新版本
     */
    String getMavenPackageLatestVersion(String groupId, String artifactId);

    /**
     * 根据包名获取maven包的GroupId和ArtifactId
     *
     * @param mdpkgName 元数据包名
     * @return maven依赖关系列表
     */
    MavenPackageRefs getGAByMdpkgName(String mdpkgName);

    /**
     * 获取单个Maven包的版本列表
     *
     * @param sourceId 包源标识
     * @param packName 包名
     * @return 版本列表
     */
    MavenComponents getVersionsOfSinglePackage(String sourceId, String packName);

    void batchOperate(String path, List<String> operations);
}
