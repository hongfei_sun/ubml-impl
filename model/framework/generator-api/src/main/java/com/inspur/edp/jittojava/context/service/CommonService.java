/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.context.service;

import java.util.List;

/**
 * Classname CommonService Description 提供获取java工程目录和jar服务。 Date 2019/8/8 16:18
 *
 * @author zhongchq
 * @version 1.0
 */
public interface CommonService {

    /**
     * @param propath 绝对路径 eg:\projects\Scm\SD\SalesOrder\bo-salesorder0722
     * @return java.lang.String eg:java\scm-sd-salesorder
     * @author zhongchq
     **/
    String getProjectPath(String propath);

    /**
     * 获取jar包所在的绝对路径
     *
     * @param propath eg:\projects\Scm\SD\SalesOrder\bo-salesorder0722
     * @return 相对路径， 例如java\scm-sd-salesorder\scm-sd-salesorder-api\target\scm-sd-salesorder-api-0.1.0-SNAPSHOT.jar
     * @throws
     * @author zhongchq
     **/
    List<String> getJarPath(String propath);
}
