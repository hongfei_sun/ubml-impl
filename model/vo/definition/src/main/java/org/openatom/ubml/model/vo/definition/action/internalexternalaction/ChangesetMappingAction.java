/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;

/**
 * The Definition Of Changeset Mapping Action
 *
 * @ClassName: ChangesetMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ChangesetMappingAction extends MappedCdpAction implements IInternalExtendAction {
	public static final String ID = "5798f884-c222-47f4-8bbe-685c7013dee4";
	public static final String CODE = "ChangesetMapping";
	public static final String NAME = "内置变更集Mapping操作";
	public ChangesetMappingAction()
	{
		setID(ID);
		setCode(CODE);
		setName(NAME);
	}
}