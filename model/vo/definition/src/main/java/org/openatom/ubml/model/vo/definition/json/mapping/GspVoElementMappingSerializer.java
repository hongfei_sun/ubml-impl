/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonGenerator;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Josn Serializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingSerializer extends ViewModelMappingSerializer{

    @Override
    protected final void writeExtendMappingProperty(JsonGenerator writer, ViewModelMapping mapping) {
        GspVoElementMapping voMapping =(GspVoElementMapping)mapping;
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.SourceType, voMapping.getSourceType().toString());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetObjectId, voMapping.getTargetObjectId());
        SerializerUtils.writePropertyValue(writer, ViewModelJsonConst.TargetElementId, voMapping.getTargetElementId());
    }
}
